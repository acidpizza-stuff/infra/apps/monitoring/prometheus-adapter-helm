locals {
  chart_version = "3.3.1"
  values = "${file("values.yaml")}"
  values_rules = "${file("values_rules.yaml")}"
  namespace = "monitoring"
}

# Prometheus Adapter
resource "helm_release" "prometheus-adapter" {
  name = "prometheus-adapter"
  namespace = local.namespace
  repository = "https://prometheus-community.github.io/helm-charts"
  chart = "prometheus-adapter"
  version = local.chart_version
  atomic = false
  create_namespace = false
  timeout = 600

  # https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/values.yaml
  # https://github.com/cablespaghetti/k3s-monitoring/blob/master/kube-prometheus-stack-values.yaml
  # dynamic "set" {
  #   for_each = {
  #     # --- Use external cert manager ---
  #     "certmanager.install" = false
  #   }
  #   content {
  #     name = set.key
  #     value = set.value
  #     type = "auto"
  #   }
  # }
  values = [
    local.values,
    local.values_rules
  ]
}
